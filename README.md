[![docs](https://img.shields.io/badge/info-documentation-informational)](https://ftag-salt.docs.cern.ch/)

# Salt

This is the home of the salt framework, which is used to train state of the art jet flavour tagger in the style of GN1.
Documentation is available [here](https://ftag-salt.docs.cern.ch/).

This repository is intended to replace the [previous repository used to train GN1](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/GNNJetTagger).
