# Introduction

Welcome to the Salt framework.
Salt is a general-purpose framework to train state-of-the art jet flavour tagging algorithms such as GN1.

The code is hosted on the CERN GitLab:

- [https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt)

You can find information about different taggers at the central FTAG algorithms [docs pages](https://ftag.docs.cern.ch/algorithms/GNN/).
Discussions about GNNs take place on Mattermost [here](https://mattermost.web.cern.ch/aft-algs/channels/gnns).

???+ info "Salt tutorial"

    A tutorial on how to use the framework is provided at the [central FTAG docs page](https://ftag.docs.cern.ch/software/tutorials/tutorial-salt/)

### Previous Frameworks

This repository will cherry pick the best bits of

- [https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/GNNJetTagger](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/GNNJetTagger)
- [https://gitlab.cern.ch/mleigh/flavour_tagging/-/blob/master/ftag/](https://gitlab.cern.ch/mleigh/flavour_tagging/-/blob/master/ftag/)

and become the default place for everyone to train models going forward.
