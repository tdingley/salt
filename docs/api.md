## Dataloading
### :::salt.data.JetDataset
### :::salt.data.JetDataModule

## Initialisation
### ::: salt.models.InitNet

## Task Heads
### ::: salt.models.TaskBase
### ::: salt.models.ClassificationTask
### ::: salt.models.RegressionTaskBase
### ::: salt.models.RegressionTask

## Components
### ::: salt.models.Dense

## Modules
### ::: salt.models.JetTagger
### ::: salt.models.MLPTagger